<!DOCTYPE html>
<head>
  <title>@yield('titulo', 'Inicio') Prueba laravel</title>
  <meta charset="utf-8">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
</head>
<body>
  @yield('contenido')
</body>
</html>
